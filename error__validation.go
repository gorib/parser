package parser

import "fmt"

type ValidationError interface {
	AddField(tag string)
	AddIndex(index int)
}

type validationError struct {
	tag string
}

func (v *validationError) AddField(tag string) {
	v.tag = fmt.Sprintf("%s->%s", tag, v.tag)
}

func (v *validationError) AddIndex(index int) {
	v.AddField(fmt.Sprintf("[%d]", index))
}
