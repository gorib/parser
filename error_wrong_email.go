package parser

import "fmt"

func NewWrongEmailError(tag string, value any) *wrongEmailError {
	return &wrongEmailError{
		validationError: &validationError{tag},
		value:           value,
	}
}

type wrongEmailError struct {
	*validationError
	value any
}

func (v *wrongEmailError) Error() string {
	return fmt.Sprintf("field '%s' has to contain email but it contains '%v'", v.tag, v.value)
}
