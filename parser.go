package parser

import (
	"encoding/json"
	"fmt"
	"net/mail"
	"reflect"
	"strings"
	"time"
)

var ErrCannotParse = fmt.Errorf("cannot parse payload")

func Parse[Destination any](body []byte, dest *Destination) (_ *Destination, err error) {
	if dest == nil {
		dest = new(Destination)
	}
	err = json.Unmarshal(body, dest)
	if err != nil {
		return nil, fmt.Errorf("%w: %v", ErrCannotParse, err)
	}
	err = validate(body, dest)
	if err != nil {
		return nil, fmt.Errorf("%w: %v", ErrCannotParse, err)
	}

	return dest, nil
}

func validate[Destination any](body []byte, dest *Destination) error {
	t := reflect.TypeOf(*dest)
	var value any
	if _, ok := getStructElem(t); ok {
		var v map[string]any
		err := json.Unmarshal(body, &v)
		if err != nil {
			return err
		}
		value = v
	} else if t.Kind() == reflect.Slice {
		if _, ok := getStructElem(t.Elem()); ok {
			var v []map[string]any
			err := json.Unmarshal(body, &v)
			if err != nil {
				return err
			}
			value = v
		}
	}
	return analyze(value, t)
}

func getStructElem(t reflect.Type) (reflect.Type, bool) {
	t = deref(t)

	if t.Kind() == reflect.Struct && !t.ConvertibleTo(reflect.TypeOf(time.Time{})) {
		return t, true
	}
	return nil, false
}

func analyze(data any, t reflect.Type) error {
	t = deref(t)

	if tVal, ok := getStructElem(t); ok {
		return analyzeStruct(data, tVal)
	} else if t.Kind() == reflect.Slice {
		if tVal, ok := getStructElem(t.Elem()); ok {
			return analyzeSlice(data, tVal)
		}
	}
	return nil
}

func analyzeSlice(data any, t reflect.Type) error {
	v := reflect.ValueOf(data)
	if v.Kind() == reflect.Invalid {
		return nil
	} else if v.Kind() != reflect.Slice {
		panic("validation: unexpected value")
	}
	for i := 0; i < v.Len(); i++ {
		err := analyzeStruct(v.Index(i).Interface(), t)
		if err != nil {
			if validationErr, ok := err.(ValidationError); ok {
				validationErr.AddIndex(i)
			}
			return err
		}
	}
	return nil
}

func analyzeStruct(data any, t reflect.Type) error {
	for i := 0; i < t.NumField(); i++ {
		tagOpts := map[string][]string{}
		for _, tag := range strings.Split(t.Field(i).Tag.Get("parse"), ",") {
			p := strings.Split(tag, ":")
			var v []string
			if len(p) > 1 {
				v = p[1:]
			}
			tagOpts[p[0]] = v
		}
		jsonTags := strings.Split(t.Field(i).Tag.Get("json"), ",")
		tagName := jsonTags[0]
		if tagName == "" || tagName == "-" {
			return nil
		}
		if len(jsonTags) > 1 {
			for _, jsonTag := range jsonTags[1:] {
				p := strings.Split(jsonTag, ":")
				var v []string
				if len(p) > 1 {
					v = p[1:]
				}
				tagOpts[p[0]] = v
			}
		}
		value, ok := data.(map[string]any)[tagName]

		// begin validation
		if _, found := tagOpts["required"]; found && (!ok || value == nil) {
			return NewMissingValueError(tagName)
		}
		if _, found := tagOpts["email"]; found && ok {
			email, valid := value.(string)
			if !valid {
				return NewWrongEmailError(tagName, value)
			}
			if _, err := mail.ParseAddress(email); err != nil {
				return NewWrongEmailError(tagName, value)
			}
		}
		// end validation

		if value == nil {
			return nil
		}
		err := analyze(value, t.Field(i).Type)
		if err != nil {
			if validationErr, ok := err.(ValidationError); ok {
				validationErr.AddField(tagName)
			}
			return err
		}
	}
	return nil
}

func deref(t reflect.Type) reflect.Type {
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	return t
}
