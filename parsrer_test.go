package parser

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestValidate(t *testing.T) {
	t.Run("Single", func(t *testing.T) {
		var d struct {
			A []struct {
				S string `json:"s" parse:"required"`
			} `json:"a" parse:"required"`
			B *struct {
				S string `json:"s" parse:"required"`
			} `json:"b" parse:"required"`
			C *[]struct {
				S string `json:"s" parse:"required"`
			} `json:"c" parse:"required"`
			D struct {
				S string `json:"s" parse:"required"`
			} `json:"d" parse:"required"`
			E *[]*struct {
				S string `json:"s" parse:"required"`
			} `json:"e" parse:"required"`
			F string     `json:"f" parse:"required"`
			G int        `json:"g" parse:"required"`
			T *time.Time `json:"t" parse:"required"`
		}

		ok(t, `{"a":[],"b":{"s":"true"},"c":[],"d":{"s":"true"},"e":[],"f":"","g":0,"t":"2022-12-12T12:00:12Z", "": 123}`, &d)
		ok(t, `{"a":[{"s":"true"}],"b":{"s":"true"},"c":[],"d":{"s":"true"},"e":[],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}`, &d)
		ok(t, `{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}`, &d)
		ok(t, `{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}`, &d)

		fail(t, `{"a":[{}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}`, &d, "cannot parse payload: field 'a->[0]->s' is required but missing")
		fail(t, `{"a":[{"s":"true"}],"b":{},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}`, &d, "cannot parse payload: field 'b->s' is required but missing")
		fail(t, `{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}`, &d, "cannot parse payload: field 'c->[0]->s' is required but missing")
		fail(t, `{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}`, &d, "cannot parse payload: field 'd->s' is required but missing")
		fail(t, `{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}`, &d, "cannot parse payload: field 'e->[0]->s' is required but missing")
		fail(t, `{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"g":0,"t":"2022-12-12T12:00:12Z"}`, &d, "cannot parse payload: field 'f' is required but missing")
		fail(t, `{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","t":"2022-12-12T12:00:12Z"}`, &d, "cannot parse payload: field 'g' is required but missing")
		fail(t, `{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0}`, &d, "cannot parse payload: field 't' is required but missing")
	})

	t.Run("List", func(t *testing.T) {
		var d []struct {
			A []struct {
				S string `json:"s" parse:"required"`
			} `json:"a" parse:"required"`
			B *struct {
				S string `json:"s" parse:"required"`
			} `json:"b" parse:"required"`
			C *[]struct {
				S string `json:"s" parse:"required"`
			} `json:"c" parse:"required"`
			D struct {
				S string `json:"s" parse:"required"`
			} `json:"d" parse:"required"`
			E *[]*struct {
				S string `json:"s" parse:"required"`
			} `json:"e" parse:"required"`
			F string     `json:"f" parse:"required"`
			G int        `json:"g" parse:"required"`
			T *time.Time `json:"t" parse:"required"`
		}

		ok(t, `[{"a":[],"b":{"s":"true"},"c":[],"d":{"s":"true"},"e":[],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d)
		ok(t, `[{"a":[{"s":"true"}],"b":{"s":"true"},"c":[],"d":{"s":"true"},"e":[],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d)
		ok(t, `[{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d)
		ok(t, `[{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d)

		fail(t, `[{"a":[{}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->a->[0]->s' is required but missing")
		fail(t, `[{"a":[],"b":null,"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->b' is required but missing")
		fail(t, `[{"a":[],"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->b' is required but missing")
		fail(t, `[{"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->a' is required but missing")
		fail(t, `[{"a":null,"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->a' is required but missing")
		fail(t, `[{"a":123,"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, `cannot parse payload: json: cannot unmarshal number into Go struct field .a of type []struct { S string "json:\"s\" parse:\"required\"" }`)
		fail(t, `[{"a":[{"s":"true"}],"b":{},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->b->s' is required but missing")
		fail(t, `[{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->c->[0]->s' is required but missing")
		fail(t, `[{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{},"e":[{"s":"true"}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->d->s' is required but missing")
		fail(t, `[{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{}],"f":"","g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->e->[0]->s' is required but missing")
		fail(t, `[{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"g":0,"t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->f' is required but missing")
		fail(t, `[{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","t":"2022-12-12T12:00:12Z"}]`, &d, "cannot parse payload: field '[0]->g' is required but missing")
		fail(t, `[{"a":[{"s":"true"}],"b":{"s":"true"},"c":[{"s":"true"}],"d":{"s":"true"},"e":[{"s":"true"}],"f":"","g":0}]`, &d, "cannot parse payload: field '[0]->t' is required but missing")
	})
}

func TestParse(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		a := struct {
			One int    `json:"one"`
			Two string `json:"two"`
		}{}
		_, err := Parse([]byte(`{"one":1,"two":"second"}`), &a)
		assert.Nil(t, err)
		assert.Equal(t, 1, a.One)
		assert.Equal(t, "second", a.Two)
	})

	t.Run("Error", func(t *testing.T) {
		a := struct {
			One int    `json:"one"`
			Two string `json:"two"`
		}{}
		_, err := Parse([]byte(`{"one":"1","two":"second"}`), &a)
		assert.Error(t, err, fmt.Sprintf("%v: json: cannot unmarshal string into Go struct field .one of type int", ErrCannotParse))
	})

	t.Run("ErrorValidation", func(t *testing.T) {
		a := struct {
			One   int    `json:"one"`
			Two   string `json:"two"`
			Three int    `json:"three" parse:"required"`
		}{}
		_, err := Parse([]byte(`{"one":1,"two":"second"}`), &a)
		assert.Error(t, err, "cannot parse payload: field 'three' is required but missing")
	})

	t.Run("ParseDate", func(t *testing.T) {
		var date time.Time
		_, err := Parse([]byte(`"2022-11-12T12:12:12Z"`), &date)
		assert.Nil(t, err)
		assert.Equal(t, "2022-11-12 12:12:12", date.Format("2006-01-02 15:04:05"))
	})

	t.Run("EmptyDest", func(t *testing.T) {
		type tst struct {
			One int    `json:"one"`
			Two string `json:"two"`
		}
		a, err := Parse([]byte(`{"one":1,"two":"second"}`), new(tst))
		assert.Nil(t, err)
		assert.Equal(t, 1, a.One)
		assert.Equal(t, "second", a.Two)
	})

	t.Run("EmptyDest", func(t *testing.T) {
		type tst struct {
			One int    `json:"one"`
			Two string `json:"two"`
		}
		a, err := Parse[tst]([]byte(`{"one":1,"two":"second"}`), nil)
		assert.Nil(t, err)
		assert.Equal(t, 1, a.One)
		assert.Equal(t, "second", a.Two)
	})

	t.Run("ToString", func(t *testing.T) {
		a, err := Parse[string]([]byte(`"{\"t\":true}"`), nil)
		assert.Nil(t, err)
		assert.NotNil(t, a)
		b, err := Parse[map[string]any]([]byte(*a), nil)
		assert.Nil(t, err)
		assert.NotNil(t, b)
		assert.Equal(t, true, (*b)["t"])
	})
}

func TestEmailValidation(t *testing.T) {
	t.Run("ValidEmail", func(t *testing.T) {
		a := struct {
			Email string `json:"email" parse:"email"`
		}{}
		_, err := Parse([]byte(`{"email":"test@example.com"}`), &a)
		assert.Nil(t, err)
		assert.Equal(t, "test@example.com", a.Email)
	})

	t.Run("InvalidEmail", func(t *testing.T) {
		a := struct {
			Email string `json:"email" parse:"email"`
		}{}
		_, err := Parse([]byte(`{"email":"invalid-email"}`), &a)
		assert.Error(t, err, "cannot parse payload: field 'email' is not a valid email address")
	})

	t.Run("EmailFieldNonString", func(t *testing.T) {
		a := struct {
			Email any `json:"email" parse:"email"`
		}{}
		_, err := Parse([]byte(`{"email":123}`), &a)
		assert.Error(t, err, "cannot parse payload: field 'email' must be a string")
	})
}

func TestMalformedJSON(t *testing.T) {
	t.Run("MalformedJSON", func(t *testing.T) {
		a := struct {
			One int `json:"one"`
		}{}
		_, err := Parse([]byte(`{"one":}`), &a)
		assert.Error(t, err, "cannot parse payload: invalid character '}' looking for beginning of value")
	})
}

func TestDeeplyNestedStructures(t *testing.T) {
	t.Run("DeeplyNested", func(t *testing.T) {
		type Level3 struct {
			Field string `json:"field" parse:"required"`
		}
		type Level2 struct {
			L3 Level3 `json:"l3" parse:"required"`
		}
		type Level1 struct {
			L2 Level2 `json:"l2" parse:"required"`
		}

		a := struct {
			L1 Level1 `json:"l1" parse:"required"`
		}{}

		ok(t, `{"l1":{"l2":{"l3":{"field":"value"}}}}`, &a)
		fail(t, `{"l1":{"l2":{"l3":{}}}}`, &a, "cannot parse payload: field 'l1->l2->l3->field' is required but missing")
	})
}

func TestLargePayload(t *testing.T) {
	t.Run("LargePayload", func(t *testing.T) {
		a := struct {
			Large string `json:"large"`
		}{}
		largeString := strings.Repeat("a", 1000000)
		payload := fmt.Sprintf(`{"large":"%s"}`, largeString)

		_, err := Parse([]byte(payload), &a)
		assert.Nil(t, err)
		assert.Equal(t, largeString, a.Large)
	})
}

func TestNilAndEmptyFields(t *testing.T) {
	t.Run("NilField", func(t *testing.T) {
		a := struct {
			Field *string `json:"field"`
		}{}
		_, err := Parse([]byte(`{"field":null}`), &a)
		assert.Nil(t, err)
		assert.Nil(t, a.Field)
	})

	t.Run("EmptyArray", func(t *testing.T) {
		a := struct {
			List []string `json:"list"`
		}{}
		_, err := Parse([]byte(`{"list":[]}`), &a)
		assert.Nil(t, err)
		assert.Empty(t, a.List)
	})
}

func TestInvalidType(t *testing.T) {
	t.Run("InvalidType", func(t *testing.T) {
		a := struct {
			One int `json:"one"`
		}{}
		_, err := Parse([]byte(`{"one":"string"}`), &a)
		assert.Error(t, err, "cannot parse payload: json: cannot unmarshal string into Go struct field .one of type int")
	})
}

func ok[T any](t *testing.T, payload string, dest *T) {
	_, err := Parse([]byte(payload), dest)
	assert.Nil(t, err)
}

func fail[T any](t *testing.T, payload string, dest *T, msg string) {
	_, err := Parse([]byte(payload), dest)
	assert.EqualError(t, err, msg)
}
