package parser

import "fmt"

func NewMissingValueError(tag string) *missingValueError {
	return &missingValueError{
		validationError: &validationError{tag},
	}
}

type missingValueError struct {
	*validationError
}

func (v *missingValueError) Error() string {
	return fmt.Sprintf("field '%s' is required but missing", v.tag)
}
